package src;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

import com.sun.prism.paint.Color;

public abstract class Formulario{

    static JButton btnCarregar = new JButton("Carregar");
    static JButton btnSair = new JButton("Sair");
    static JFrame quadro = new JFrame("Pagina do Rubs");
    static JPanel painel = new JPanel();
    static JTable tabela = new JTable();

    static int sizeTelax = 500;
    static int sizeTelay = 500;

    static JScrollPane scroll = new JScrollPane();

    static ArrayList<String> linhas = new ArrayList<String>();

    public static void montarTela(){
        prepararBotoes();
        prepararPainelPrincipal();
        preparaTabela();
        prepararQuadroPrincipal();
        mostrarTela();
    }

    public static void prepararQuadroPrincipal(){
        quadro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quadro.add(painel);

        quadro.pack();
        
    }

    public static void prepararPainelPrincipal(){
        
        painel.add(scroll);
        painel.add(btnCarregar);
        painel.add(btnSair);            
    }
    
    public static void prepararBotoes(){
        // botao sair...
        btnSair.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // botao carregar arquivo...
        btnCarregar.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{                    
                    File arquivo;
                    JFileChooser filer = new JFileChooser("C:/RUBS/2018.1/Tecnicas/TF/TESTES");

                    // aceitamos somente txt
                    filer.setFileFilter(new FileNameExtensionFilter("Somente arquivos de texto", "txt"));

                    if(filer.showOpenDialog(null) == filer.APPROVE_OPTION){
                        arquivo = filer.getSelectedFile();

                        FileReader arq = new FileReader(arquivo);
                        BufferedReader buffer = new BufferedReader(arq);

                        String linha = null;
                        linha = buffer.readLine();

                        while(linha != null){    
                            linhas.add(linha);
                            linha = buffer.readLine();
                        }

                        arq.close();
                        // eh preciso dar um update no painel
                        prepararPainelPrincipal();
                        
                    }else{
                        JOptionPane.showMessageDialog(null, "Erro ao abrir o arquivo!");
                    }
                    // 
                }catch(Exception exp){
                    System.out.println("nao consegui achar o diretorio...");
                    exp.printStackTrace();
                }
            }
        });
    }

    public static void mostrarTela(){
        quadro.setSize(sizeTelax, sizeTelay);        
        quadro.setVisible(true);
    }

    public static void preparaTabela(){
                
        tabela.setBorder(new LineBorder(java.awt.Color.black));
        tabela.setGridColor(java.awt.Color.black);
        tabela.setShowGrid(true);
        //tabela.setTableHeader(new JTableHeader());
        scroll.setAutoscrolls(true);
        scroll.setRowHeader(null);
        scroll.getViewport().setBorder(null);
        scroll.getViewport().add(tabela);
        scroll.setSize(sizeTelax, sizeTelay);
        
        for(String s : linhas){
            linhas.add(s);
        }
        Tabela tab = new Tabela(linhas);
        
        tabela.setModel(tab);
    }
}