package src;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class Tabela extends AbstractTableModel{
    
    ArrayList <String> lista = new ArrayList<String>();

    public Tabela( ArrayList <String> l){
        lista = l;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return lista.get(rowIndex % getRowCount());
    }
}