package src;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;

public class Tela{
    
    private int width = 500, height = 500;
    JFrame quadro = new JFrame("Estatistix - Rubens");

    public Tela(){}
    public Tela(int w, int h){
        width = w;
        height = h;
    }

    public start(){
        preparaBotoes();
        preparaLayout();
        mostrarTela();
    }
    
    // implementações

    /*
    essa funcao deve mostrar a tela e setar qualquer detalhe dela
    */
    private void mostrarTela(){
        quadro.setSize(width, height);
        quadro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quadro.pack();
        quadro.setVisible(true);
    }

    /* 
    Essa funcao deve preparar o layout, adicionar os outros componentes, 
    dar todo o design e espacamentos da tela.
    */

    private void preparaLayout(){
        quadro.add(preparaPainelMenus());
        quadro.add(preparaPainelSide());
        quadro.add(preparaPainelPrincipal());
    }

    private JPanel preparaPainelSide(){
        JPanel painel = new JPanel();
        // pegar as operacoes no objeto de operacoes e colocar para cada uma um botao que seta a operacao da vez...
        // ver documentacao
        return painel;
    }

    /*
        O painel principal tem tem os paineis de interacao, de mostrar tabela e mostrar resultado
    */
    private JPanel preparaPainelPrincipal(){
        JPanel painel = new JPanel();
        // pegar as operacoes no objeto de operacoes e colocar para cada uma um botao que seta a operacao da vez...
        // ver documentacao
        return painel;
    }

    private JPanel preparaPainelMenus(){
        
        JPanel painel = new JPanel();

        // dar um laout a esse bixo... cada menu deve ser adicionado lado a lado
        List <JMenu> menus = preparaMenus();
        for (JMenu m : menus) {
            painel.add(m);          
        }
        return painel;
    }

    private List<JMenu> preparaMenus(){
        List <JMenu> menus = new List <JMenu> ();
        // do something
        return menus;
    }

    private void preparaBotoes(){
        /*
            Essa funcao inicializa e seta os botoes do formulario.
            Botao de carregar a tabela da memoria
            Botao de Executar operacao
        */
        JButton carregaTabela = new JButton("Ok");
        JButton executaOp = new JButton("Ok");

        executaOp.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                // deve escolher uma operacao dentro das possiveis e mandar executar
            }
        });

        carregaTabela.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                // deve carregar a tabela provavelmente na view... ver diagrama
            }
        });
    }

    private void preparaTabela(){
        carregaTabela();
    }
}