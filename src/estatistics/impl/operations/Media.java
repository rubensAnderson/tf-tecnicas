/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl.operations;
import java.util.List;

import estatistics.impl.*;


/**
 * 
 * An implementation of the model object '<em><b>Media</b></em>'.
 * 
 *
 * 
 */
public class Media extends Operation {
	/**
	 * 
	 * 
	 * 
	 */
	public Media() {
		name = "MEDIA";
		numParam = 1;
	}
	

	/**
	 * Soma os valores de cada linha e divide pelo tamanho dela. 
	 * Depois soma as medias de cada linha e divide pelo numero de linhas. (media das medias)
	 * @param tab Tabela a qual sera feita a media. 
	 * @return resultado do tipo ResUnivariado (um numero)
	 */
	public Resultado processar(List <Tupla <String> > tab){
		if(tab == null || tab.size() < numParam){
            return new ResUnivariado("Erro! numero de colunas selecionadas insuficiente!");            
        }
		Resultado res;
		try{
			res = new ResUnivariado( String.valueOf(OpTab.media(tab.get(0))));
		}catch(NumberFormatException e){
			res = new ResUnivariado("Erro! Nao foi possivel converter o dado da tupla para numeral");
		}
		return res;
	}



	@Override
	public int getNumParam() {
		return numParam;
	}
}