/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl.operations;

import java.util.List;

import estatistics.impl.*;


public class Barras extends Operation{
    public Barras(){
		name = "BARRAS";
		numParam = 1;
    }

	@Override
	public Resultado processar(List<Tupla <String> > t1) {
		if(t1 == null || t1.size() < numParam){
            return new ResUnivariado("Erro! numero de colunas selecionadas insuficiente!");            
        }
		Resultado res = new ResGrafico( (OpTab.barras(t1.get(0))));
		
		return res;
	}

	@Override
	public int getNumParam() {
		return numParam;
	}
}