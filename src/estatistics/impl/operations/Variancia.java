/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl.operations;

import java.util.List;


import estatistics.impl.*;

public class Variancia extends Operation{
    public Variancia(){
        name = "VARIANCIA";
        numParam = 1;
    }

	public Resultado processar(List <Tupla <String> > tab){
		if(tab == null || tab.size() < numParam){
            return new ResUnivariado("Erro! numero de colunas selecionadas insuficiente!");            
        }
		Resultado res;
		try{
			res = new ResUnivariado( String.valueOf(OpTab.variancia(tab.get(0))));
		}catch(NumberFormatException e){
			res = new ResUnivariado("Erro! Nao foi possivel converter o dado da tupla para numeral");
		}
		return res;
	}

	@Override
	public int getNumParam() {
		return numParam;
	}
    
}