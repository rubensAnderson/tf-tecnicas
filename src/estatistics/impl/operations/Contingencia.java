/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl.operations;
import java.util.List;

import estatistics.impl.*;

public class Contingencia extends Operation{
    public Contingencia(){
		name = "CONTINGENCIA";
		numParam = 2;
    }

	@Override
	public Resultado processar(List<Tupla <String> > tab) {
		if(tab == null || tab.size() < numParam){
            return new ResUnivariado("Erro! numero de colunas selecionadas insuficiente!");            
        }
		Resultado res;
		res = new ResTabela(OpTab.contingencia( tab.get(0) , tab.get(1)));
		return res;
	}

	@Override
	public int getNumParam() {
		return numParam;
	}
}