/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl.operations;

import java.util.List;

import estatistics.impl.*;


public class TabFreqQuanti extends Operation{
    public TabFreqQuanti(){
        name = "TABFREQQUANTI";
    }

	// !!! IMPLEMENTAR !!!

	@Override
	public Resultado processar(List <Tupla <String> > tab){
		if(tab == null || tab.size() < numParam){
            return new ResUnivariado("Erro! numero de colunas selecionadas insuficiente!");            
        }
		Resultado res;
		res = new ResTabela( OpTab.tabFreq(tab.get(0)));
		
		return res;
	}

	@Override
	public int getNumParam() {
		return numParam;
	}
}