/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.util.Hashtable;

/**
 * 
 * An implementation of the model object '<em><b>View Observer</b></em>'.
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.ViewObserverImpl#getTela <em>Tela</em>}</li>
 *   <li>{@link estatistics.impl.ViewObserverImpl#getFacade <em>Facade</em>}</li>
 * </ul>
 *
 * 
 */
public abstract class ViewObserver extends Observer{
	/**
	 * The cached value of the '{@link #getTela() <em>Tela</em>}' reference.
	 * The cached value of the '{@link #getFacade() <em>Facade</em>}' reference.
	 * 
	 * 
	 * @see #getTela()
	 * @see #getFacade()
	 * 
	 * @ordered
	 */

	protected Tela tela;
	protected static Hashtable<String, Facade> facade = new Hashtable<String, Facade>();
	
	//===============================getters==========================================

	public Tabela getTabela(String idTabela){
		System.out.println("- view pegando a tabela...");
		return facade.get(idTabela).getTabela(idTabela);
	}
	//===============================negocio==========================================

	public abstract void atualizar(String novotipo);
	/**
	* start deve inicializar a tela.
	*/
	public void start(){
		System.out.println("- view Startando...");
	}
	
	
	/**
	 * 
	 * <p> manda o modelo selecionar uma nova tabela atraves do facade </p>
	 * 
	 */
	public void atualizaTabela(String idTabela) {
		System.out.println("- view Atualizando a tabela...");
		if(!facade.containsKey(idTabela)){
			facade.put(idTabela, AbstractFactoryFacade.createFacadeQuanti());
		}
		facade.get(idTabela).update(idTabela);
	}

	@Override
	public String toString() {
		return "";
	}

} 
