/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import java.util.List;

/**
 * 
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.OperationImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * 
 */
public abstract class Operation{
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * 
	 * 
	 * @see #getName()
	 * 
	 * @ordered
	 */
	protected int numParam = 1;
	protected String name = "";


	/**
	 * 
	 * @return retorna o nome da operacao.
	 * 
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * <p> seta o nome da operacao </p>
	 * 
	 * @param newName nome da operacao
	 */
	public void setName(String newName) {
		name = newName;
	}

	/**
	 * 
	 * <p> Cada funcao deve implementar seu processamento </p>
	 * 
	 * @return retorna um dado do tipo Resultado de acordo com o tipo de dado
	 */

	//public abstract Resultado processar(Tabela tab);
	
	public abstract Resultado processar(List <Tupla <String> > tab);
	public abstract int getNumParam();
	
}
