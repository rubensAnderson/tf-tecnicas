

package estatistics.impl;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class geradorGraficos{
    protected Tupla <String> t1;
    protected Tupla <String> t2;

    public JPanel gerar(Tupla <String> tt1, Tupla <String> tt2){
        t1 = tt1;
        t2 = tt2;
        DefaultCategoryDataset dataset1 = new DefaultCategoryDataset();
        for(int i = 0; i < tt1.getTamanho(); i++) {
        	dataset1.addValue((Number) Double.parseDouble(tt1.get(i)), i, 0);        	
        }
        JFreeChart grafico = ChartFactory.createLineChart("cu", "x", "y", dataset1);
        
        return new ChartPanel(grafico);
    }
}