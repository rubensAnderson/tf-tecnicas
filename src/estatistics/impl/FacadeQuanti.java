/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * 
 *
 * 
 */
public class FacadeQuanti extends Facade{
	/**
	 * 
	 * 
	 * 
	 */
	protected FacadeQuanti() {
		System.out.println("- Criando a facadeQuanti...");
		conjoperations = AbstractFactoryConjOperation.createConjOperationQuanti();
	}

	@Override
	public String toString() {
		return "facade quantitativa";
	}

}
