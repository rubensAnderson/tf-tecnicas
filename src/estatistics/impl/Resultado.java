/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.util.ArrayList;

import java.util.Hashtable;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * 
 * 
 */
public abstract class Resultado{
	
	/**
	 * The cached value of the '{@link #getInter() <em>Inter</em>}' attribute.
	 * 
	 * 
	 * @see #getInter()
	 * 
	 * @ordered
	 */
	protected JPanel painel = new JPanel();
	protected Hashtable<String , JComponent> componentes = new Hashtable<String , JComponent>();
	
	public JPanel getPainel() {
		return painel;
	}

	/**
	 * 
	 * 
	 * 
	 */
	public JComponent getComponente(String id) {
		if(!componentes.containsKey(id))
			return null;
		return componentes.get(id);
	}
	/**
	 * 
	 * 
	 * 
	 */
	public ArrayList <JComponent> getComponentes() {
		ArrayList <JComponent> l = new ArrayList<JComponent>();
		for (String nome : componentes.keySet()) {
			l.add(componentes.get(nome));
		}

		return l;
	}

	/**
	 * 
	 * 
	 * 
	 */
	public void putComponente(String id, JComponent c) {
		
		componentes.put(id, c);
		
	}
} 
