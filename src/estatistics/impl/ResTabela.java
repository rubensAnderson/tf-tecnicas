/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * 
 * An implementation of the model object '<em><b>Tabela Res</b></em>'.
 * 
 *
 * 
 */
public class ResTabela extends Resultado {

	protected JTable tabela = new JTable();
	/**
	 * 
	 * 
	 * 
	 */
	public ResTabela(Tabela tab) {
		System.out.println("=============setando a restabela============");
		tabela.setModel(tab);
		componentes.put(tab.getId(), tabela);
		
		//tabela.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabela.setBorder(new LineBorder(Color.gray));
        tabela.setGridColor(Color.gray);
        tabela.setShowGrid(true);
        tabela.setShowVerticalLines(false);
        tabela.setPreferredSize(new Dimension(1000, 600));
        tabela.setPreferredScrollableViewportSize(new Dimension(800, 500));
        
        DefaultTableCellRenderer centerer = new DefaultTableCellRenderer();
    	centerer.setHorizontalAlignment(SwingConstants.CENTER);
    	centerer.setPreferredSize(new Dimension(40,20));
    	
    	for(int i = 0; i < tabela.getColumnCount(); i++) {
    		tabela.getColumnModel().getColumn(i).setPreferredWidth(35);
    		//tabela.getColumnModel().getColumn(i).setWidth(45);
    		tabela.getColumnModel().getColumn(i).setCellRenderer(centerer);
    		tabela.getColumnModel().getColumn(i).setHeaderRenderer(centerer);
			tabela.getColumnModel().getColumn(i).setHeaderValue(tab.getIndice().get(i));
    	}
		
    	//tabela.getColumnModel().getColumn(0).setPreferredWidth(50);
    	
		JScrollPane scrol = new JScrollPane();
		
		scrol.getViewport().add(tabela);
		scrol.setHorizontalScrollBar(new JScrollBar(JScrollBar.HORIZONTAL));
		scrol.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));
		scrol.setPreferredSize(new Dimension(800, 500));
		painel.add(scrol);
		
	}

}
