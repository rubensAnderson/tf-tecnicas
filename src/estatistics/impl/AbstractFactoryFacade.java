/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * 
 * 
 *
 * 
 */
public abstract class AbstractFactoryFacade{

	/**
	 * 
	 * <p> retorna uma nova facade quali. </p>
	 * 
	 * 
	 */
	public static FacadeQuali createFacadeQuali() {
		return new FacadeQuali();
	}

	/**
	 * 
	 * <p> retorna uma nova facade quali. </p>
	 * 
	 * 
	 */
	public static FacadeQuanti createFacadeQuanti() {
		return new FacadeQuanti();
	}
	
	/**
	 * 
	 * <p> retorna uma nova facade quali. </p>
	 * 
	 */
	public static FacadeAll createFacadeAll() {
		return new FacadeAll();
	}

	
	public static Facade create(String tipo){
		if(tipo == "QUANTI"){
			return createFacadeQuanti();
		}
		else if(tipo == "QUALI"){
			return createFacadeQuali();
		}
		return createFacadeAll();
	}

} 
