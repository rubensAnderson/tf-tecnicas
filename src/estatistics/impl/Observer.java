/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;


/**
 * 
 * <p> Observer do Padrao de projetos Observer. </p>
 * 
 * 
 * 
 */
public abstract class Observer {

	protected Observable modelo;
	/**
	 * 
	 * Quando algo muda no modelo, o observer deve ser notificado
	 * Observer do padrao de projetos Observer
	 * a facade muda, entao os filhos devem olhar esse detalhe
	 */	
	public abstract void notifyObs();

	/**
	 * 
	 * Para ser notificado, ele deve primeiro se inscrever no observavel
	 * @see #notifyObs()
	 */
	public void subscribe() {
		System.out.println("- observer se inscrevendo...");
		modelo.attatch(this);
	}

} 
