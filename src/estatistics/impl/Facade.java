/**
 * Essa eh a faixada da view em relacao as funcoes internas do sistema.
 * Eh atraves dela que a view pode ter acesso ao modelo e executar operacoes.
 *
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.Facade#getTabelaController <em>TabelaController</em>}</li>
 *   <li>{@link estatistics.impl.Facade#getResultado <em>Resultado</em>}</li>
 * </ul>
 *
 * 
 */
public abstract class Facade{

	/**
	 * The cached value of the '{@link #getResultado() <em>Resultado</em>}' reference.
	 * 
	 * 
	 * @see #getResultado()
	 * 
	 * @ordered
	 */
	
	protected Resultado resultado;
	protected ConjOperations conjoperations;

	// ======================================getters e setters==============================================

	/**
	 * @return Retorna o resultado da ultima operacao. 
	 * <p> pode retornar um valor nulo, caso nao tenha ocorrido nenhuma operacao sobre a tabela atual. </p>
	 * 
	 */
	public Resultado getResultado() {
		System.out.println("- facade pegando resultado...");
		return resultado;
	}

	/**
	 * @return Tabela cujo id eh idTabela
	 */
	public Tabela getTabela(String idTabela){
		System.out.println("- facade pegando tabela..." + idTabela);
		return TabelaController.getTabela(idTabela);
	}
	/**
	 * 
	 */
	public ConjOperations getConjOperations() {
		System.out.println("- TabelaController pegando conj operacoes...");
		return conjoperations;
	}

	// ======================================Negocio===============================================

	public void atualizaConjOperation(String tipo){
		System.out.println("== Atualizando conjunto de operacoes ==");
		conjoperations = AbstractFactoryConjOperation.create(tipo);
	}
	/**
	 * <p> Repassa o arquivo csv para o controlador poder atualizar a tabela. </p>
	 * 
	 * @param arquivo da nova tabela
	 */
	public void update(String idTabela) {
		System.out.println("- facade atualizando tabela..." + idTabela);
		TabelaController.changeContext(idTabela);
	}

	/**
	 * <p> Manda o controlador executar a operacao passada, e esta deve constar na relacao de operacoes.
	 * 
	 * @param nome da operacao
	 * @return um dado do tipo Resultado
	 */
	
	public Resultado processar(String idTabela, String operationName, List<Integer> ind) {
		System.out.println("- facade processando... " + idTabela);
		
		Tabela tab = TabelaController.getTabela(idTabela);
		ArrayList <Tupla <String> > tuplas = new ArrayList<Tupla <String> >();
		
		for(int i : ind) {
			tuplas.add(tab.getCol(i));
		}
		
		return conjoperations.getOperation(operationName).processar(tuplas);
	}

	public String inferDataType(String idTabela, int i){
		System.out.println("- facade inferindo..." + idTabela + ".col " + i);
		return TabelaController.inferDataType(idTabela, i);
	}
	
	public String inferDataType(String idTabela){
		System.out.println("- facade inferindo..." + idTabela);
		return TabelaController.inferDataType(idTabela);
	}

	// ========================================Observer=============================================

	/**
	 * 
	 * 
	 * 
	 */
	public void attatch(String id, Observer obs) {
		System.out.println("- facade inscrevendo obs..." + id);
		TabelaController.attatch(id, obs);
	}

	/**
	 * 
	 * 
	 * 
	 */
	public void dettatch(String id, Observer obs) {
		TabelaController.dettatch(id, obs);		
	}
	
}
