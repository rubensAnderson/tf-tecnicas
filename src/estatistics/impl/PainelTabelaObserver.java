
/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import javax.swing.JPanel;

/**
 * 
 * An implementation of the model object '<em><b>PainelTabelaObserver</b></em>'.
 * 
 *
 * 
 */
public class PainelTabelaObserver extends estatistics.impl.ViewObserver{

    protected JPanel painel;
    protected String id = "0";
    protected String tipo = "ALL";
    protected Tabela tabela;

	//===============================inicio=============================================
	
	/**
     *	inicializador da PainelTabelaObserver
	* Inicializa a tabela e a facade supondo dados quantitativos. 
	* quando a tabela for inserida, ele decide qual tipo de dado realmente eh.
	*/
	public PainelTabelaObserver(String ide){
        System.out.println("- Iniciando O painel da tabela..." + ide);
        id = ide;
        
        //tipo = "QUANTI";

        if(!facade.containsKey(ide)){
            facade.put(ide, AbstractFactoryFacade.createFacadeAll());
        }
        
        facade.get(ide).attatch(id, this);
        tabela = new Tabela(ide);
    }
    
    //===============================getters e setters=============================================

    public Tabela getTabela(){
        return tabela;
    }
    //===============================negocio=============================================

	/**
	 * callback do botao de alterar a tabela no formulario.
	 */
	public void change(){
		facade.get(id).update(id);
	}

	/** 
	 * Esta funcao muda o tipo de dados, a sua facade e sua tabela. 
	 * Ela supoe que o tipo de dados mudou.
	 * @param novotipo Novo tipo de dados que pode ser "QUANTI": quantitativo ou "QUALI": qualitativo.
	*/
    public void atualizar(String novotipo){
        System.out.println("- painel tabela atualizando...");
        String tt = "ALL";
        tipo = novotipo;
        tt = tipo;       
        
        facade.put(id, AbstractFactoryFacade.create(tt));
        facade.get(id).attatch(id, this);              
    }
    
    /**
	* implementacao do metodo abstrato da classe Observer
	* 
	*/
	public void notifyObs(){
        System.out.println("==========voltando a view===========");
        System.out.println("- painel tabela sendo notificada...");
        String novotipo = facade.get(id).inferDataType(id);
        
        System.out.println("-- Tabela: novo tipo: " + novotipo);
        System.out.println("-- Tabela: antigo tipo: " + tipo);
        
        if(novotipo.equals(tipo)){
            atualizar(novotipo);            
        }
        tabela = facade.get(id).getTabela(id);
	}
	
}