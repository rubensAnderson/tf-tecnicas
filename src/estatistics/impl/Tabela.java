/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.filechooser.FileNameExtensionFilter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * 
 * An implementation of the model object '<em><b>Tabela</b></em>'.
 * 
 *
 * 
 */
public class Tabela extends Observable {
	static final long serialVersionUID = 36876432;
	
	/**
	 * A tabela tem uma lista de tuplas
     */
	protected String titulo = "";
	protected String id = "0";
	
	protected Tupla <String> indice = new Tupla<String>();
	protected Tupla <String> tipos = new Tupla <String>();
	
    protected List<Tupla <String> > tuplas = new LinkedList<Tupla <String> >();
	protected final String quali = "QUALI", quanti = "QUANTI";
	
	protected int tamCol = 0, tamLin = 0;
	
	//===============================inicio====================================
	
	public Tabela(String idd) {
		id = idd;
	}
	
	public Tabela(int tami, int tamj){
		tamLin = tami;
		tamCol = tamj;
	}

	public Tabela(String idd, int tami, int tamj){
		id = idd;
		tamLin = tami;
		tamCol = tamj;
	}
	
	public Tabela(String idd, String tituloo) {
		id = idd;
		titulo = tituloo;
	}
	
	//===============================getters e setters====================================
	
	public String getId(){
		return id;
	}

	/**
	 * 
	 * @param s
	 * @param i
	 */
	public void setIndice(String s, int i){
		if(tuplas == null) {
			tuplas = new ArrayList<Tupla<String> >();
		}
		
		if(tuplas.size() != 0) {
			tamCol = tuplas.get(0).getTamanho();
		}else {
			tamCol = i + 1;
		}
		
		if(i >= tamCol) {
			System.out.println("!!! i out of bounds !!! (Tabela::setINdice)");
			return;
		}
		if (indice == null){
			System.out.println("!!! indices nulos !!! (Tabela::setIndice)");
			indice = new Tupla<String>();			
		}
		
		indice.put(s, i);
	}

	/**
	 * pega uma linha inteira da tabela
	 * @param i
	 * @return
	 */
	public Tupla <String> getTupla(int i){
		if(tuplas == null || tamLin <= i) {
			System.out.println("!!! pegando linha out of bounds !!!");
			return new Tupla <String>();
		}
		return tuplas.get(i);
	}
	
	/**
	 * pega todas as linhas da tabela
	 * @return
	 */
	public List<Tupla <String> > getTuplas(){
		return tuplas;
	}

	/**
	 * <p> O tamanho das colunas eh dado de
	 * tres formas:
	 * <ul>
	 * 		<li> se a tabela <i><b>tem uma tupla</b></i>, entao o tamCol = tam(tupla(0))
	 * 		<li> senao se a tabela <i><b>tem o indice definido</b></i>, entao o tamCol = tam(indice)
	 * 		<li> senao a tabela esta <i><b>zerada</b></i>. A primeira tupla inserida da o tamanho das colunas : tamCol = tam(indice)
	 * </ul>
	 * </p>
	 * Adiciona uma tupla no final da tabela
	 * @param t Tupla
	 */
	public void addTupla(Tupla <String> t){
		if(t == null) {
			System.out.println("!!! tupla nula !!! (Tabela.addTupla)");
			return;
		}
		if(tuplas == null) {
			tuplas = new ArrayList<Tupla<String> >();
			System.out.println("!!! tuplas nulas !!! (Tabela.addTupla)");
//			/return;
		}
		
		//constraints
		if(tuplas.size() != 0)	
			tamCol = tuplas.get(0).getTamanho();
		else if(indice.getTamanho() != 0)
			tamCol = indice.getTamanho();
		else
			tamCol = t.getTamanho();
		
		if(t.getTamanho() != tamCol) {
			System.out.println("!!! tamanho da coluna incompativel!!! (Tabela::addTupla)");
			return;
		}
		
		if(!tuplas.contains(t)) {
			tuplas.add(t);
			tamLin++;
		}
	}

	/**
	 * 
	 * @return os nomes das colunas
	 */
	public Tupla <String> getIndice(){
		return indice;
	}
	
	/**
	 * Pega a coluna i inteira da tabela
	 * @param i
	 * @return
	 */
	public Tupla <String> getCol(int i) {
		
		Tupla <String> res = new Tupla <String> ();
		
		if(tuplas == null) {
			System.out.println("!!! tuplas nulas !!! (Tabela::getCol)");
			return res;
		}
		if(i >= tamCol){
			System.out.println("!!! indice passado out of bounds !!! (Tabela::getCol)");
			return getCol(tamCol - 1);
		}

		for (Tupla <String> t : tuplas) {
			res.put(t.get(i));
		}
		return res;
	}

	private void setCol(int i){
		while(i >= tamCol){
			setLastCol();
		}
	}

	private void setLastCol(){
		if(tuplas == null){
			System.out.println("!!! tuplas nulas !!! (setLastCol)");
			return;
		}
		indice.put(" ");
		for (Tupla <String> t : tuplas) {
			t.put(" ");
		}
		tamCol++;
	}
	
	/**
	 * 
	 * 
	 * @param tipo tipo de dado requerido
	 * @return lista com as colunas que tem esse tipo
	 */
	public List<Tupla <String> > getAllCollByType(String tipo){
		ArrayList <Tupla <String> > res = new ArrayList<Tupla <String> > ();
		if(tipos == null) {
			return res;
		}
		
		for(int i = 0; i < tipos.getTamanho(); i++) {
			if (tipos.get(i) == tipo) {
				res.add(getCol(i));
			}
		}
		return res;
	}

	@Override
    public int getColumnCount() {
		
        return tamCol;
    }

	@Override
    public int getRowCount() {
		if(tuplas == null) {
			return 0;
		}
		tamLin = tuplas.size();
		return tamLin;
	}
	
	
    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
    	if(tuplas == null) {
    		System.out.println("!!! tuplas nulas !!! (Tabela::getValueAt)");
    		return "";
    	}
    	if(rowIndex >= tamLin) {
    		System.out.println("!!! row out of bounds !!! (Tabela::getValueAt)");
    		return "";
    	}
    	if(columnIndex >= tamCol) {
    		System.out.println("!!! col out of bounds !!! (Tabela::getValueAt)");
    		return "";
    	}
    	
    	return tuplas.get(rowIndex).get(columnIndex);
	}
	

	
	
	//===============================negocio====================================
	/**
	 * <p> Abre o selecionador de arquivos do SO e seleciona um arquivo csv. <br>
	 *   Armazena:
	 *   <ul> 
		 *   <li> nas tuplas cada linha de dados da tabela </li>
		 *   <li> o nome de cada atributo </li> 
		 *   <li> os tipos de cada coluna. </li>
	 *   </ul>
	 * </p>
	 */
	public void update(){
		System.out.println("- tabela atualizando dados...");
		
		try{                    
			File arquivo;
            JFileChooser filer = new JFileChooser("C:/RUBS/2018.1/Tecnicas/TF");
		
            filer.setFileFilter(new FileNameExtensionFilter ("Somente arquivos csv", "csv"));
			
            if(filer.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
				arquivo = filer.getSelectedFile();
				
                FileReader arq = new FileReader(arquivo);
                BufferedReader buffer = new BufferedReader(arq);
				
                indice = new Tupla<String>();
                tuplas = new ArrayList<Tupla<String> >();
                
                String linha = null;
				linha = buffer.readLine();
				// eh necessario inserir o indice antes
				if(linha != null){
					indice.put(linha.split(","));
				}else{
					System.out.println("Arquivo vazio...");
					buffer.close();
					return;
				}

				
				linha = buffer.readLine();
                while(linha != null){                	
					Tupla <String> t = new Tupla<String>(linha.split(","));
					tuplas.add(t);                  
					linha = buffer.readLine();
                }
				
                arq.close();
				tamCol = tuplas.get(0).getTamanho();
				tamLin = tuplas.size();
                inferDataType();			
				
                // notificar observadores...
				notifyObs();
                
            }else{
                JOptionPane.showMessageDialog(null, "Erro ao abrir o arquivo!");
            }
            // 
        }catch(Exception exp){
            System.out.println(" *** Nao consegui abrir o arquivo ***");
            exp.printStackTrace();
        }
	}
	
	public String inferDataType() {
		Tupla <String> tips = getTipos();
		if(tips == null) {
			return quali;
		}
		
		if(tips.contains(quali)) {
			if(tips.contains(quanti)) {
				return "ALL";
			}
			return quali;
		}
		return quanti;
	}
	
	/**
	 * 
	 * Infere o tipo de dado das colunas. Podem ser do tipo
	 * <b>QUALI</b> ou <b>QUANTI</b>
	 * @return Tupla de Strings que representa o tipos de dados das colunas.
	 * 
	 */
	public Tupla <String> getTipos(){
		System.out.println("== Inferindo dados da tabela toda ==");
		if(indice == null || tuplas == null) {
			return tipos;
		}
		tipos = new Tupla<String>();
		for(int i = 0; i < indice.getTamanho(); i++) {
			tipos.put(inferDataType(i));
		}
		return tipos;
	}
	
	/**
	 * 
	 * Infere o tipo de dado da coluna i. Pode ser do tipo
	 * <b>QUALI</b> ou <b>QUANTI</b>
	 * @param i indice do atributo
	 * @return String que representa o tipo de dado
	 * 
	 */
	public String inferDataType(int i) {
		System.out.println("- tabela inferindo em " + i);
		if (tuplas == null){
			//throw new nullStateException();
			return quanti;
		}
		
		boolean isquanti = true;
		//SString patternNum  = "[0-9]";
		//String patternPonto  = ".";
		for (Tupla <String> t : tuplas) {
			
			System.out.println("---- "+(String)t.get(i));
					
			if(Pattern.matches("[a-zA-Z_0-9]", (String)t.get(i)) && !Pattern.matches("[0-9]", (String)t.get(i))) {
				System.out.println("!!! 1 nao eh numeral !!! (" + (String)t.get(i) + ")");
				isquanti = false;
				break;
			}
			
			String r[] = ((String)t.get(i)).split("");
			
			/*
			for(int k = 0; k < r.length; k++)
				System.out.println("---- "+r[k]);
			*/
			for(String c : r) {
				if(!Pattern.matches("[0-9]", c) && !Pattern.matches(".", c)) {
					System.out.println("!!! 2 nao eh numeral !!! (" + c + ")");
					isquanti = false;
					break;
				}
			}
			if(!isquanti) {
				break;
			}
		}
		
		if(isquanti) {
			tipos.put(quanti, i);
			System.out.println("---- " + quanti);
			return quanti;
		}
		System.out.println("---- " + quali);
		tipos.put(quali, i);
		return quali;
	}

	@Override
	public String toString() {
		String res = "indice: " + indice.toString();
		for(Tupla <String> t : tuplas) {
			res += "\n"+t.toString();
		}
		return res;
		
	}
} 
