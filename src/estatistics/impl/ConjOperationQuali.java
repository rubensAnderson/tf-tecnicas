/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import estatistics.impl.operations.*;

/**
 * 
 * <p> Classe que representa um conjunto de operacoes qualitativas. </p>
 * 
 *
 * 
 */
public class ConjOperationQuali extends ConjOperations {
	/**
	 * 
	 * Operacoes qualitativas: 
	 * 
	 */
	public ConjOperationQuali() {
		operation.add(new TabFreqQuali());
		operation.add(new Contingencia());
		operation.add(new Barras());
		// ...
	}

}
