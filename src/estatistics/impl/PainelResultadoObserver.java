
/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import java.util.List;

/**
 * 
 * An implementation of the model object '<em><b>PainelResultadoObserver</b></em>'.
 * 
 *
 * 
 */
public class PainelResultadoObserver extends estatistics.impl.ViewObserver{
    
    protected String id = "0";
    protected String tipo = "ALL";
    protected Resultado res = new ResUnivariado("0");
	//===============================inicio=============================================
	
	/**
     *	inicializador da PainelResultadoObserver
	* Inicializa a tabela e a facade supondo dados quantitativos. 
	* quando a tabela for inserida, ele decide qual tipo de dado realmente eh.
	*/
	public PainelResultadoObserver(String ide){
        System.out.println("- Iniciando O painel do resultado..." + ide);
        id = ide;
		
        //tipo = "QUANTI";

        if(!facade.containsKey(ide)){
            facade.put(ide, AbstractFactoryFacade.createFacadeAll());
        }
        
        facade.get(ide).attatch(id, this);
        // NAO TEM O QUE POR NO RESULTADO AINDA. ESTAR PREPARADO PARA VALOR NULO NO INICIO
    }
    
    //===============================negocio=============================================

    /**
     * Retorna um cu cagado
     */
    public Resultado getResultado(){
        return res;
    }
    
    //===============================negocio=============================================

    /**
	 * callback do botao de processar a operacao sobre a tabela no formulario.
	 */
	public void processar(List <Integer> ind, String op){
		System.out.println("== Painel resultado processando ==");
		res = facade.get(id).processar(id, op, ind);
    }
    
	public void atualizar(String novotipo){
        String tt = "ALL";
    	tipo = novotipo;
        tt = tipo;	
        
        facade.put(id, AbstractFactoryFacade.create(tt));
        facade.get(id).attatch(id, this);
        facade.get(id).atualizaConjOperation(tt);
        res = facade.get(id).getResultado(); // pode vir nulo...
    }

	    
	/**
	* implementacao do metodo abstrato da classe Observer
	* 
	*/
	public void notifyObs(){
        System.out.println("==========voltando a view===========");
        System.out.println("- painel resultado sendo notificada...");
        String novotipo = facade.get(id).inferDataType(id);
        System.out.println("-- Resultado: novo tipo: " + novotipo);
        System.out.println("-- Resultado: antigo tipo: " + tipo);
        if(novotipo != tipo){
            atualizar(novotipo);            
        }
	}
}