/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import javax.swing.JPanel;

/**
 * 
 * An implementation of the model object '<em><b>Grafico</b></em>'.
 * 
 *
 * 
 */
public class ResGrafico extends Resultado {

	/**
	 * 
	 * 
	 * 
	 */
	public ResGrafico(JPanel p) {
		painel = p;
	}
}
