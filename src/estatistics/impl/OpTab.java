/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;

import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.BoxAndWhiskerToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


/**
 * <p> Repositorio de funcoes sobre tuplas e tabelas </p>
 * 
 * Funcoes implementadas:
 * <ul>
 *  <li> soma de tuplas </li>
 * </ul>
 */
public abstract class OpTab{

    //=======================Basics=========================

    /**
     * soma duas tuplas
     * @param t1
     * @param t2
     * @return tupla
     */
    public static Tupla <String> soma(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException{

        
        if(t1 == null | t2 == null){
            JOptionPane.showMessageDialog(null, "Erro! Tem tupla nula!(OperationsTabela::soma)");
            return null;
        }
        if(t1.getTamanho() != t2.getTamanho()){
            JOptionPane.showMessageDialog(null, "Erro! Soma recebe duas tuplas de tamanhos iguais (OperationsTabela::soma)");
            return null;
        }
        
        Tupla <String> res = new Tupla <String> ();

        try{
            for(int i = 0; i < t1.getTamanho(); i++){
                res.put( String.valueOf( Double.parseDouble(t1.get(i)) + Double.parseDouble(t2.get(i)) ));
            }
        }catch(Exception e){
            System.out.println("!!! Talvez nao seja um double hein !!!");
        }

        return res;
    }

    /**
     * Multiplica uma tupla por um valor numerico
     * @param t Tupla
     * @param v valor
     */
    public static Tupla <String> mulByVal(Tupla <String> t, double v) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na mulbyval !!! (OpTab::mulByVal)");
    		return new Tupla <String>();
        }
        
        Tupla <String> res = new Tupla <>();

        for(int i = 0; i < t.getTamanho(); i++){
            res.put( String.valueOf(Double.parseDouble(t.get(i)) * v));
        }
        return res;
    }

    /**
     * Soma uma tupla por um valor numerico
     * @param t Tupla
     * @param v valor
     */
    public static Tupla <String> sumByVal(Tupla <String> t, double v) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na sumbyval !!! (OpTab::sumbyval)");
    		return new Tupla <String>();
        }
        
        Tupla <String> res = new Tupla <>();

        for(int i = 0; i < t.getTamanho(); i++){
            res.put( String.valueOf( Double.parseDouble(t.get(i)) + v));
        }
        return res;
    }
    public static double E(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na E !!! (OpTab::E)");
    		return 0;
        }
       
        double total = 0;
        for (String v : t.getAll()) {
            total += Double.parseDouble(v);
        }
        return total;        
    }

    /**
     * Recebe uma tupla e retorna a soma dos quadrados dela
     */
    public static double square(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na square !!! (OpTab::)");
    		return 0;
        }
        
        double total = 0;
        for (String v : t.getAll()) {
            total += Math.pow( Double.parseDouble(v) , 2);
        }
        return total;
    }

    public static Tupla <String> exp(Tupla <String> t, int e){
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na exp !!! (OpTab::exp)");
    		return new Tupla<String>();
        }
        
        Tupla <String> total = new Tupla <>();
        for (String v : t.getAll()) {
            total.put( String.valueOf(Math.pow( Double.parseDouble(v), e)));
        }
        return total;
    }

    public static Tupla<String> mul(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException{
        
        if(t1 == null | t2 == null){
            JOptionPane.showMessageDialog(null, "Erro! Tem tupla nula!(OpTab::mul)");
            return new Tupla <String>();
        }
        if(t1.getTamanho() != t2.getTamanho()){
            JOptionPane.showMessageDialog(null, "Erro! Soma recebe duas tuplas de tamanhos iguais (OpTab::mul)");
            return new Tupla<String>();
        }

        if(t1.getTamanho() != t2.getTamanho()){
            return null;
        }
        Tupla <String> res = new Tupla<>();
        for(int i = 0; i < t1.getTamanho(); i++){
            res.put( String.valueOf( Double.parseDouble(t1.get(i)) * Double.parseDouble(t2.get(i))));
        }

        return res;
    }

    //=======================Terceirizando===========================
    /**
     * Terceirizando o trabalho... media
     */
    public static double media(Tupla <String> t) throws NumberFormatException{
        
    	if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na media !!! (OpTab::Media)");
    		return 0;
        }
        
    	if(t.getTamanho() == 2) {
    		return ( Double.parseDouble( t.get(0)) + Double.parseDouble( t.get(1)))/2;
    	}
        
        return E(t) / t.getTamanho();
    }
    
    public static double minimo(Tupla <String> t) throws NumberFormatException{
    	if(t == null || t.getTamanho() == 0){
    		System.out.println("!!! erro no minimo !!! (OpTab::minimo)" );
            return 0;
        }
    	
    	Vector <Double> v = new Vector <Double>();
    	for(int i = 0; i < t.getTamanho(); i++){
    		if(t.get(i) != "" || t.get(i) != " ")	
    			v.add(Double.parseDouble(t.get(i)));
    	}
    	
    	v.sort(Comparator.naturalOrder());
    	
    	return v.firstElement();
    }
    
    public static double maximo(Tupla <String> t) throws NumberFormatException{
    	if(t == null || t.getTamanho() == 0){
    		System.out.println("!!! erro no minimo !!! (OpTab::maximo)" );
            return 0;
        }
    	
    	Vector <Double> v = new Vector <Double>();
    	for(int i = 0; i < t.getTamanho(); i++){
    		if(t.get(i) != "" || t.get(i) != " ")
    			v.add(Double.parseDouble(t.get(i)));
    	}
    	
    	v.sort(Comparator.naturalOrder());
    	
    	return v.lastElement();
    }
    
    public static String moda(Tupla <String> t)throws NumberFormatException {
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na moda !!! (OpTab::moda)");
    		return "NaN";
        }
        
    	if(t == null || t.getTamanho() == 0){
            return "NaN";
        }
    	
        Hashtable <String, Integer>  ht = new Hashtable <>();
        for (int i = 0; i < t.getTamanho(); i++) {
            // do something
            String o = (t.get(i));
            if(!ht.containsKey(o)){
                ht.put(o, 0);
            }
            ht.put(o, ht.get(o) + 1);                
        }
        

        Vector <Integer> moda = new Vector<Integer>();
        for(int i : ht.values()) {
        	moda.add(i);
        }
        
        // o ultimo elemento deve ser o que aparece mais
        moda.sort(Comparator.naturalOrder());
        // pega o elemento que mais aparece
        int max = moda.lastElement();
        for(String d : ht.keySet()) {
        	if(ht.get(d) == max) {
        		return d;
        	}
        }
        
        return "NaN";        
    }
    
    /**
     * Retorna o elemento na metade do vetor. Com programacao dinamica ficaria mas barato, mas nao eh pra tanto.
     * 
     * @param t
     * @return mediana
     */
    public static double mediana(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na mediana !!! (OpTab::Mediana)");
    		return 0;
        }
        
        if(t.getTamanho() == 1) {
        	return Double.parseDouble(t.get(0));
        }
        
        Vector <Double> v = new Vector<Double>();
        
        for(String s : t.getAll()) {
        	if(s == "" || s == " ")
        		s = "0";
        	v.add( Double.parseDouble(s) );
        }
        
        v.sort(Comparator.naturalOrder());
        
        System.out.println();
        
        if(v.size() % 2 == 0) {
        	double v1 = (v.get( Math.floorDiv(v.size(), 2) ));
        	double v2 = (v.get( Math.floorDiv(v.size(), 2) - 1));
        	return (v1 + v2)/2;
        }
        return (v.get( Math.floorDiv(v.size(), 2) ));
        
    }
    

    /**
     * Terceirizando o trabalho... population kurtosis formula
     */
    public static double kurtosis(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na kurtosis !!! (OpTab::kurtosis)");
    		return 0;
        }
       
        int n =  t.getTamanho();

        return (E(exp( sumByVal(t, -media(t)) , 4)))/ (n * Math.pow(desvioPadrao(t), 4));
    }

    /**
     * Terceirizando o trabalho... population skewness formula
     * 
     * 
     */
    public static double skewness(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na skewness !!! (OpTab::)");
    		return 0;
        }
        
        if(t == null || t.getTamanho() == 0){
            return 0;
        }

        int n =  t.getTamanho();

        return (E(exp( sumByVal(t, -media(t)) , 3)))/ (n * Math.pow(desvioPadrao(t), 3));
    }

    
	public static Tupla <String> mediaSimples(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException{
        return OpTab.mulByVal(OpTab.soma(t1, t2), 0.5);
    }

    /**
     * Vaariancia...
     */
    public static double variancia(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na variancia !!! (OpTab::varianccia)");
    		return 0;
        }
        
        if(t == null || t.getTamanho() <= 1){
            return 0;
        }
        return E( exp( sumByVal(t, -media(t)) , 2) ) / (t.getTamanho() - 1);
    }
    

    public static double desvioPadrao(Tupla <String> t) throws NumberFormatException{
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na desviopadrao !!! (OpTab::)");
    		return 0;
        }
        
        return Math.pow(variancia(t), 0.5);
    }
    

    public static double covariancia(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException{
    	if(t1 == null | t2 == null){
            System.out.println("!!! Tem tupla nula!!! (OpTab::covariancia)");
            return 0;
        }
        if(t1.getTamanho() != t2.getTamanho()){
            System.out.println("Erro! Soma recebe duas tuplas de tamanhos iguais (OpTab::covariancia)");
            return 0;
        }
        int n = t1.getTamanho();

        return ( E(mul(t1, t2)) - (1/n) * (E(t1) * E(t2)))/n;
    }

    /**
     * Coeficiente de correlacao de Pearson
     */
    public static double coefCorrel(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException{
    	if(t1 == null | t2 == null){
            System.out.println("Erro! Tem tupla nula!(OpTab::coefCorrel)");
            return 0;
        }
        if(t1.getTamanho() != t2.getTamanho()){
            System.out.println( "Erro! Soma recebe duas tuplas de tamanhos iguais (OpTab::coefCorrel)");
            return 0;
        }
        return covariancia(t1, t2) / Math.pow(variancia(t1) * variancia(t2), 0.5);
    }

    public static JPanel scatterPlot(Tupla<String> t1, Tupla<String> t2) throws NumberFormatException{
    	if(t1 == null | t2 == null){
            System.out.println("Erro! Tem tupla nula!(OpTab::scater)");
            return null;
        }
        if(t1.getTamanho() != t2.getTamanho()){
            JOptionPane.showMessageDialog(null, "Erro! Soma recebe duas tuplas de tamanhos iguais (OpTab::scater)");
            return null;
        }

        XYDataset dataset = createDataset(t1, t2);

        JFreeChart chart = ChartFactory.createScatterPlot(
        "Scater Plot", 
        "X", "Y", dataset);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(255,228,196));

        ChartPanel panel = new ChartPanel(chart);

        return panel;
    }

    public static JPanel histogram(Tupla <String> t) throws NumberFormatException {
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na histogram !!! (OpTab::histogram)");
    		return new JPanel();
        }
        
        double[] value = new double[t.getTamanho()];
        
        for (int i=0; i < t.getTamanho(); i++)
            value[i] = Double.parseDouble(t.get(i));
            
        int number = t.getTamanho();
        
        HistogramDataset dataset = new HistogramDataset();
        dataset.setType(HistogramType.FREQUENCY);
        dataset.addSeries("Histograma",value,number);
        String plotTitle = "Histograma"; 
        String xaxis = "";
        String yaxis = ""; 
        PlotOrientation orientation = PlotOrientation.VERTICAL; 
        boolean show = false; 
        boolean toolTips = true;
        boolean urls = false; 
        final JFreeChart chart = ChartFactory.createHistogram( plotTitle, xaxis, yaxis, 
                 dataset, orientation, show, toolTips, urls);
    
        ChartPanel chartpanel = new ChartPanel(chart);

        return chartpanel;
    }

    @SuppressWarnings("deprecation")
    public static JPanel Boxplot(Tupla <String> t) throws NumberFormatException {
        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na boxplot !!! (OpTab::)");
    		return new JPanel();
        }
        
        final BoxAndWhiskerCategoryDataset dataset = createDatasetBox(t);

        final CategoryAxis xAxis = new CategoryAxis("");
        final NumberAxis yAxis = new NumberAxis("Value");
        yAxis.setAutoRangeIncludesZero(false);
        final BoxAndWhiskerRenderer renderer = new BoxAndWhiskerRenderer();
        renderer.setFillBox(false);
        renderer.setToolTipGenerator(new BoxAndWhiskerToolTipGenerator());
        final CategoryPlot plot = new CategoryPlot(dataset, xAxis, yAxis, renderer);

        final JFreeChart chart = new JFreeChart(
            "BoxPlot",
            plot
        );
        ChartPanel chartPanel = new ChartPanel(chart);

        return chartPanel;
    }

    // ========================datasets=========================

    private static XYDataset createDataset(Tupla <String> t1, Tupla <String> t2) throws NumberFormatException {
    	if(t1 == null | t2 == null){
            System.out.println("Erro! Tem tupla nula!(OpTab::dataset)");
            return null;
        }
        if(t1.getTamanho() != t2.getTamanho()){
            System.out.println("Erro! Soma recebe duas tuplas de tamanhos iguais (OpTab::dataset)");
            return null;
        }

        //========================================================
        
        XYSeriesCollection dataset = new XYSeriesCollection();
    
        XYSeries series1 = new XYSeries("");

        for(int i = 0; i < t1.getTamanho(); i++){
            series1.add( Double.parseDouble(t1.get(i)), Double.parseDouble(t2.get(i)));
        }
    
        dataset.addSeries(series1);

        return dataset;
    }

    private static DefaultBoxAndWhiskerCategoryDataset createDatasetBox(Tupla <String> t) throws NumberFormatException {

        if(t == null || t.getTamanho() == 0) {
            System.out.println("!!! deu erro na databox !!! (OpTab::createDataBox)");
    		return new DefaultBoxAndWhiskerCategoryDataset();
        }
        
        ArrayList<String> inputDataList = new ArrayList<String>();
        for (int i=0;i<t.getTamanho();i++)
            inputDataList.add(t.get(i));

        final DefaultBoxAndWhiskerCategoryDataset dataset 
            = new DefaultBoxAndWhiskerCategoryDataset();
        dataset.add(inputDataList, "1", "2");

        return dataset;
    }

    /**
     * qualitativo, mas se aplica a quanti tbm.
     */
    public static Tabela contingencia(Tupla <String> t1, Tupla <String> t2){
    	System.out.println("====================inicio contingencia====================");
        Tabela res = new Tabela("0");
        
        if(t1 == null || t2 == null || t1.getTamanho() <= 0 || t2.getTamanho() <= 0 ||  t1.getTamanho() != t2.getTamanho()){
            System.out.println("!!! Tuplas invalidas !!! (contingencia)");
            return res;
        }
        
        
        Hashtable<String, Integer> cont = new Hashtable<String, Integer>();
        
        // contando...
        String concat = "";
        String regex = "___________";
        for(int i = 0; i < t1.getTamanho(); i++){
        	concat = t1.get(i) + regex + t2.get(i);
        	if(!cont.containsKey(concat)){
        		cont.put(concat, 0);
        	}
        	
        	cont.put(concat, cont.get(concat) + 1);
        }
        
        //criando tabela
        Hashtable<String, Integer> ind1 = indexesTotais(t1);
        Hashtable<String, Integer> ind2 = indexesTotais(t2);
        
        System.out.println(ind1);
        System.out.println(ind2);
        
        Hashtable<Integer, String> row1 = new Hashtable<Integer, String>();
        Hashtable<Integer, String> col2 = new Hashtable<Integer, String>();
        
        int i = 0;
        for(String s1 : ind1.keySet()) {
        	row1.put(i, s1);
        	i++;
        }
         
        i = 0;
        res.setIndice(" ", 0);
        
        for(String s1 : ind2.keySet()) {
        	col2.put(i, s1);
        	res.setIndice(s1, i + 1);
        	i++;
        }
        res.setIndice("total", ind2.keySet().size() + 1);
        
        System.out.println(row1);
        System.out.println(col2);       
        
        Tupla <String> t = new Tupla <String>();
        
        
        for(i = 0; i < row1.keySet().size(); i++) {
            t = new Tupla <String>();
            t.put(row1.get(i));
        	for(int j = 0; j < col2.keySet().size(); j++) {
        		String key = row1.get(i) + regex + col2.get(j);
        		if(!cont.containsKey(key)){
        			t.put("0");
        		}else{
        			t.put( String.valueOf(cont.get(key)));        		
        		}
        	}
        	// total...
        	t.put( String.valueOf( ind1.get( row1.get(i) )));
        	
        	res.addTupla(t);
        }
            
        
        System.out.println(res.toString());
        
        Tupla <String> tot = new Tupla <String>();
        tot.put("Total");
        for(i = 1; i < res.getColumnCount(); i++) {
        	int ttt = (int)E(res.getCol(i));
        	tot.put(String.valueOf(ttt));
        }
        
        res.addTupla(tot);
        
        System.out.println("===================fim contingencia======================");
        return res;
    }
    
    /**
     * Realiza a contagem da <b><i>frequencia absoluta</i></b>de cada objeto que aparece na tupla t1.
     * @param t1
     * @return
     */
    private static Hashtable<String, Integer> indexesTotais(Tupla <String> t1) {
    	Hashtable<String, Integer> res = new Hashtable<String, Integer>();
    	
    	for(String s : t1.getAll()) {
    		if(!res.containsKey(s)) {
    			res.put(s, 0);
    		}
    		res.put(s, res.get(s) + 1);
    	}
    	
    	return res;
    }
    
    /**
     * 
     * @param tt1
     * @return
     */
    public static JPanel barras(Tupla <String> tt1){
    
    	JPanel painel = new JPanel();
    	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    	PlotOrientation orientation;
    	
    	
    	Hashtable<String, Integer> p = indexesTotais(tt1);
    		
		for(String s : p.keySet()){
			dataset.addValue(p.get(s), p.get(s), s );
		}    		
		orientation = PlotOrientation.HORIZONTAL;
		 
    	JFreeChart grafico = ChartFactory.createBarChart("Grafico de Barras",
    	 "x",
    	  "y", 
    	  dataset, 
    	  orientation, 
    	  false, 
    	  false, 
    	  false);
    	
    	
    	
    	painel.add(new ChartPanel(grafico));
    	
    	return painel;
    
    }
    
    
    public static Tabela tabFreq(Tupla <String> t1){
    	Tabela res = new Tabela("1");
    	
    	if(t1 == null || t1.getTamanho() == 0) {
    		System.out.println("!!! Tupla nula!!! (OPTab::tabFreq)");
    		return res;
    	}
    	
    	Hashtable<String, Integer> ind = indexesTotais(t1);
    	
    	String freqs[] = {"Frequencia Absoluta", "frequencia relativa"};
    	
    	res.setIndice("Objeto", 0);
    	res.setIndice(freqs[0], 1);
    	res.setIndice(freqs[1], 2);
    	
    	double total = 0;
    	for(String s : ind.keySet()){
    		total += ind.get(s);
    	}
    	
    	for(String s : ind.keySet()){
    		int abs = ind.get(s);
    		double rel = (double)(abs) / total;
    		Tupla <String> t = new Tupla<String>();
    		t.put(s);
    		t.put(String.valueOf(abs));
    		t.put(String.valueOf( NumberFormat.getPercentInstance().format(rel)));
    		res.addTupla(t);
    	}
    	
    	return res;
    
    }
    
}