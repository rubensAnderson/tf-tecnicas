/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Hashtable;


/**
 * 
 * An implementation of the model object '<em><b>Tela</b></em>'.
 * 
 *
 * 
 */
public class Tela{

	
    // lista de labels que vao aparecer na interface...
    protected static ArrayList <JLabel> labels = new ArrayList<JLabel>();
    
    // lista de botoes radio que vao aparecer na interface...
    protected static ArrayList <JRadioButton> botoes = new ArrayList<JRadioButton>();

    // lista de cores
    protected static final int dimGray = 105; // principal
    protected static final int gray = 128; // secundaria
    protected static final int darkGray = 169; // terciaria
    protected static final int corLabels = 255; // cor dos labels

    // tabela principal
    JTable tabela = new JTable();

    // scrol da tabela
    JScrollPane scrollTabela = new JScrollPane();

    // janela principal
    JFrame janela = new JFrame();

    /*
        painel principal. depende de:
            - painel interact
            - painel tabela
            - painel resultado
    */
    JPanel principal = new JPanel();

    // painel side. nele que vai ficar o grupo de botoes das operacoes
    JPanel side = new JPanel();
    JPanel resultado = new JPanel();
    JPanel selAtrib = new JPanel();
    ButtonGroup bg = new ButtonGroup();

    // label do side
    JLabel lop = new JLabel("OPERACOES");
    JLabel lRes = new JLabel("RESULTADO");
    JLabel ltab = new JLabel("TABELA");
    
    // dimensoes da tela, caso nao pack()
    protected int width = 500, height = 300;
    
    JPanel interact = new JPanel();
    
    JLabel trocTabela = new JLabel("Trocar Tabela");
    JLabel execOperacao = new JLabel("Executar Operacao");
    
    JButton btnTrocarTabela = new JButton("OK");
    JButton btnProcessar = new JButton("OK");

    // wrapper: ele vai abrigar o painel principal e o side
    JPanel wrapper = new JPanel();

    PainelResultadoObserver resObs;
    PainelTabelaObserver tabObs;
    PainelOperationObserver opObs;
    
    ArrayList <JCheckBox> selAtribArray = new ArrayList<JCheckBox>();
    Hashtable<Integer, String> selectedAtrib = new Hashtable<Integer, String>();

    String id = "0";
    
    
    //=====================Inicio=========================

	/**
     * Instancia os paineis observadores necessarios. 
	 * Quem dos paineis obs for instanciado primeiro cria a entrada na tabela de facades. os outros
     * se inscrevem quando sao criados
	 */
	public Tela(int w, int h, String ide){
        width = w; height = h;
        id = ide;

        resObs = new PainelResultadoObserver(id);
        tabObs = new PainelTabelaObserver(id);
        opObs = new PainelOperationObserver(id);
        start();
    }
    
    private void start(){
    		try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        setarBotoes();
    
        setarPainelSide();
    
        setarPainelPrincipal();
    
        setarWrapepr();
    
        setDesign();
    
        setarJanela();
    }
	
	private void setarWrapepr(){
		
        wrapper.add(side);
        
        wrapper.add(principal);
    }

    private void setarJanela(){
        janela.setBounds(100, 0, width, height);
        JScrollPane scrolWrapper = new JScrollPane();
        scrolWrapper.getViewport().add(wrapper);
        janela.add(scrolWrapper);

        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.pack();

        janela.setVisible(true);
    }

    //======================painel principal========================

    private void setarPainelPrincipal(){
    
    	principal.removeAll();
        setarPainelInteract();
        principal.add(interact);
        
        setarPainelTabela();
        principal.add(selAtrib);
        principal.add(scrollTabela);
        
    }
    private void setarPainelInteract(){
    	labels = new ArrayList<JLabel>();
        // labels:
        labels.add(trocTabela);
        labels.add(execOperacao);

        interact.add(trocTabela);
        interact.add(btnTrocarTabela);
        
        interact.add(execOperacao);
        interact.add(btnProcessar);    
    }
    
    /**
        Essa funcao inicializa e seta os botoes do formulario.
        Botao de carregar a tabela da memoria
        Botao de Executar operacao
    */
    private void setarBotoes(){

        btnProcessar.addActionListener(new ActionListener(){      
            @SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
        
            public void actionPerformed(ActionEvent e) {
                // deve escolher uma operacao dentro das possiveis e mandar executar
                System.out.println("========== Tela processando===========");
                
                // operacao selecionada no painel side
                String op = bg.getSelection().getActionCommand();
                System.out.println("--Operacao selecionada: " + op);
                
                // pegando os atributos selecionados
                // so pode lidar com duas colunas de cada vez. entao se tiver marcado 3 ele pega somente as duas primeiras
                int totalAtr = 0;
                selectedAtrib = new Hashtable <Integer, String>();
                // lista de atributos selecionados
                for(int i = 0; i < selAtribArray.size(); i++) {
                	if(selAtribArray.get(i).isSelected()) {
                		selectedAtrib.put(i, selAtribArray.get(i).getName());
                		totalAtr++;
                		if(totalAtr >= opObs.getNumParam(id, op)) {
                			System.out.println("deve sair com " + totalAtr);
                			break;
                		}
                	}
                } 
                
                System.out.println("-- No Atributos selecionados: " + selAtribArray.size());
                // as chaves sao os indices dos atributos selecionado
                
                ArrayList chaves = new ArrayList<>();
                for(int i : selectedAtrib.keySet()) {
                	System.out.println("---- chave: " + i);
                	chaves.add(i);
                }
                
                System.out.println("-- chaves: " + chaves.toString());
                
                resObs.processar(chaves, op);
                if(resObs.getResultado() != null) {
                	System.out.println("== Resultado diferente de nulo ==");
                	//JOptionPane jop = new JOptionPane(resObs.getResultado().getPainel());
                	//jop.setAutoscrolls(true);
                	//jop.setPreferredSize(new Dimension(1200, 800));
                	//JOptionPane.showMessageDialog(null,  jop, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                	JOptionPane.showMessageDialog(janela, resObs.getResultado().getPainel());                	
                }else {
                	
                	System.out.println("!!!Resultado igual de nulo");
                }
                
                System.out.println("== Fim do processamento ==");
                janela.setVisible(true);
            }
            
        });
        
		
		// Botao de trocar tabela
		btnTrocarTabela.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {            	
                System.out.println("========== Tela Trocando tabela===========");
                tabObs.change();
                
                System.out.println("==========change callback terminou===========");
                Tabela t = tabObs.getTabela();
                if(t.getRowCount() == 0){
                    System.out.println("!!! tabela nula !!!");
                }

                selAtrib = new JPanel();
                selAtribArray = new ArrayList<JCheckBox>();
                
                for(String indice : t.getIndice().getAll()) {
                	JCheckBox c = new JCheckBox(indice);
                	c.setName(indice);
                	c.setText(indice);
                	
                	selAtribArray.add(c);
                	selAtrib.add(c);
                }
                
                //principal.add(selAtrib);
                tabela = new JTable();
                tabela.setModel(t);
                
                setarPainelSide();
                setarPainelPrincipal();
                
                
                setDesign();
                janela.setVisible(true);
                
                System.out.println("===============btnTrocar terminou==================");
            }
        });
    }

    //======================painel side========================
    
    private void setarPainelSide(){        
    	System.out.println("== setando painel side ==");
        side.removeAll();
    	//side = new JPanel();
        // PARA FACILITAR NO DESIGN
    	lop.setText("OPERACOES");
    	lop.setForeground(new Color(corLabels, corLabels, corLabels));
        labels.add(lop);
        
        side.add(lop);
        
        if(opObs.getConjOperations() == null || opObs.getConjOperations().getOperations() == null) {
        	return;
        }
        
        System.out.println("--No Operacoes: " + opObs.getConjOperations().getOperations().size());

        botoes = new ArrayList<JRadioButton>();
        
        bg = new ButtonGroup();
        
        for (Operation op : opObs.getConjOperations().getOperations()) {
        	System.out.println("--operacao: " + op.getClass().getSimpleName());
            JRadioButton b = new JRadioButton();
            b.setText(op.getName());
            b.setActionCommand(op.getName());
            b.setToolTipText("");
            b.setRolloverEnabled(false);
            b.setContentAreaFilled(false);
            bg.add(b);
            bg.setSelected(b.getModel(), true);
            
            // ajudar no design
            botoes.add(b);
            side.add(b);
        }
        side.setVisible(true);
    }

    //====================Tabela==========================
    
    private void setarPainelTabela(){
        System.out.println("==========setando painel tabela===========");
        labels.add(ltab);

        principal.add(ltab);

        tabela.setModel(tabObs.getTabela());
        scrollTabela.getViewport().add(tabela);
    }


    //====================design==========================
    
    private void setDesign(){
        
        setDesignInteract();
        setDesignPrincipal();
        setDesignScrollTabela();
        setDesignSide();
        setDesignTabela();
        setDesignWrapper();
        
        
        for (JLabel l : labels) {
            l.setForeground(new Color(corLabels, corLabels, corLabels));
        	
        }

        for (JRadioButton b : botoes) {
           b.setBackground(new Color(0.0f, 0.0f, 0.0f, 0.0f));
           b.setForeground(new Color(corLabels, corLabels, corLabels));
        }

        principal.setLayout(new BoxLayout(principal, BoxLayout.Y_AXIS));
    }

    private void setDesignScrollTabela(){
        scrollTabela.getViewport().setBorder(null);
        
        //scrollTabela.setSize((int)(width * 0.7), (int)(height * 0.5));
        scrollTabela.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));
        scrollTabela.setHorizontalScrollBar(new JScrollBar(JScrollBar.HORIZONTAL));
    }
    
    
    
    
    private void setDesignTabela(){
    	
    	// render que poe as coisas no centro...
    	DefaultTableCellRenderer centerer = new DefaultTableCellRenderer();
    	centerer.setHorizontalAlignment(SwingConstants.CENTER);
    	
    	for(int i = 0; i < tabela.getColumnCount(); i++) {
    		tabela.getColumnModel().getColumn(i).setPreferredWidth(25);
    		tabela.getColumnModel().getColumn(i).setCellRenderer(centerer);
    		tabela.getColumnModel().getColumn(i).setHeaderValue(selAtribArray.get(i).getText());
    	}
    	
    	tabela.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabela.setBorder(new LineBorder(Color.white));
        tabela.setGridColor(Color.gray);
        tabela.setShowGrid(true);
        tabela.setShowVerticalLines(false);
    }
    
    
    
    
    
    private void setDesignInteract(){
        interact.setBackground(new Color(dimGray, dimGray, dimGray));
        interact.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }
    private void setDesignWrapper(){
    	
        wrapper.setBackground(new Color(darkGray, darkGray, darkGray));
    }
    private void setDesignPrincipal(){
    	
        principal.setBackground(new Color(darkGray, darkGray, darkGray));
        principal.setSize((int)(width * 0.7), height);
        principal.setBorder(BorderFactory.createLineBorder(Color.white));
    }

    private void setDesignSide(){
        side.setBackground(new Color(dimGray, dimGray, dimGray));
        side.setSize((int)(width * 0.3), height);
        side.setBorder(BorderFactory.createLineBorder(Color.gray));
        side.setLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
    }

} 
