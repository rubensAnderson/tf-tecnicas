/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * <p> </p>
 * 
 *
 * 
 */
public class FacadeQuali extends Facade{
	/**
	 * 
	 * 
	 * 
	 */
	protected FacadeQuali() {
		System.out.println("- Criando a facadeQuali...");
		conjoperations = AbstractFactoryConjOperation.createConjOperationQuali();
	}

	@Override
	public String toString() {
		return "facade qualitativa";
	}

}
