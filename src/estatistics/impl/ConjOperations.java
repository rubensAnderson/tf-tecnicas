/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * An implementation of the model object '<em><b>Abstract Conj Operation</b></em>'.
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.OperationImpl#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * 
 */
public abstract class ConjOperations {
	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference list.
	 * 
	 * 
	 * @see #getOperation()
	 * 
	 * @ordered
	 */
	protected List<Operation> operation = new LinkedList<Operation>();
	
	/**
	 * 
	 * 
	 * 
	 */
	public List<Operation> getOperations() {
		return operation;
	}

	/**
	 * 
	 * <p> nao achou a operacao... entao lancar uma excessao... (implementar) </p>
	 * 
	 * 
	 */

	public Operation getOperation(String nome){
		for(int i = 0; i < operation.size(); i++){
			if(operation.get(i).getName() == nome){
				return operation.get(i);
			}
		}
		return null;
		//throw new OperationNotFoundException(nome);
	}

	/**
	 * 
	 * 
	 * 
	 */

	public void setOperation(Operation op){
		if(!operation.contains(op)){
			operation.add(op);
		}
	}
	
	
	public int getNumParam(String op) {
		return getOperation(op).getNumParam();
	}

}