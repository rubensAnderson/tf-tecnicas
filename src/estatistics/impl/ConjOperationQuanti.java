/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import estatistics.impl.operations.*;

/**
 * 
 * <p> Classe que representa um conjunto de operacoes quantitativas. </p>
 * 
 *
 * 
 */
public class ConjOperationQuanti extends ConjOperations {
	/**
	 * 
	 * 
	 * 
	 */
	public ConjOperationQuanti() {
		//univariadas...
		operation.add(new Media());
		operation.add(new Mediana());
		operation.add(new Moda());
		operation.add(new Minimo());
		operation.add(new Maximo());
		operation.add(new Skewness());
		operation.add(new Kurtosis());
		operation.add(new Variancia());
		operation.add(new DesvioPadrao());
		//...
		//graficos
		operation.add(new Barras()); // contagem dos atributos -> quanti e quali
		operation.add(new ScatterPlot()); // somente quanti
		operation.add(new Histograma()); // quanti e quali
		operation.add(new BoxPlot());

		operation.add(new TabFreqQuanti());
	}

}
