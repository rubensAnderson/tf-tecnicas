/**
 * Controlador da tabela do sistema
 * 
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * An implementation of the model object '<em><b>Tabela Controller</b></em>'.
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.TabelaControllerImpl#getconjoperations <em>conjoperations</em>}</li>
 *   <li>{@link estatistics.impl.TabelaControllerImpl#getTabela <em>Tabela</em>}</li>
 *   <li>{@link estatistics.impl.TabelaControllerImpl#getResultado <em>Resultado</em>}</li>
 * </ul>
 *
 */
public abstract class TabelaController {
	/**
	 * '{@link #getTabela() <em>Tabela</em>}' reference.
	 *
	 * '{@link #getconjoperations() <em>conjoperations</em>}' reference.
	 * Conjunto de operacoes do controlador. Deve estar em acordo com o tipo de dados,
	 * e o tipo do facade
	 * 
	 * '{@link #getResultado() <em>Resultado</em>}' reference.
	 * O resultado armazenado da ultima operacao executada.
	 
	 * @see #getconjoperations()
	 * @see #getTabela()
	 * @see #getResultado()
	 * 
	 * @ordered
	 */


	/**
	 * 
	 * TabelaController deve ser inicializado junto com o programa principal
	 * Deveria ser abstrata? acho que sim, Pois so serve como repositorio de funcoes e repasse de argumentos.
	 * pensar nisso depois... HEY VOCE NAO DEVE LER ISSO JHKSDHSDHDHJSK
	 * 
	 */
	 
	// ============================getters e setters=====================================================

	/**
	 * @return Retorna a tabela atual 
	 * 
	 * 
	 */
	public static Tabela getTabela(String idTabela) {	
		System.out.println("- controller pegando tabela..." + idTabela);	
		return Model.getTabela(idTabela);
	}

	// ================================negocio=====================================================

	/**
	 * Repassa o csv para a tabela manter as regras de negocio e avisar os observers.
	 * @param arquivo csv
	 */
	public static void changeContext(String id) {
		System.out.println("- controller mudando contexto..." + id);
		Model.update(id);
	}

	// ===========================Observer==========================================================

	/**
	 * <p> Repassa o observer para a tabela poder inseri-lo </p>
	 * 
	 * @param arquivo Observer
	 */
	public static void attatch(String id, Observer obs) {
		System.out.println("- controller inscrevendo obs..." + id);
		Model.getTabela(id).attatch(obs);
	}

	/**
	 * <p> Repassa o observer para a tabela poder remove-lo </p>
	 * 
	 * @param arquivo Observer
	 */
	public static void dettatch(String id, Observer obs) {
		System.out.println("- controller desinscrevendo obs..." + id);
		Model.getTabela(id).dettatch(obs);
	}


	public static String inferDataType(String idTabela){
		System.out.println("- controller inferindo o tipo de dados..." + idTabela);
		return Model.getTabela(idTabela).inferDataType();
	}
	
	public static String inferDataType(String idTabela, int i){
		System.out.println("- controller inferindo o tipo de dados..." + idTabela);
		return Model.getTabela(idTabela).inferDataType(i);
	}
} 
