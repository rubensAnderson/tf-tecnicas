package estatistics.impl;

import java.util.ArrayList;
import java.util.List;

public class Tupla <T extends String>{
    protected List <T> atributos = new ArrayList<T> ();

    public Tupla(List <T> atrib){
        atributos = atrib;
    }

    public Tupla(T atrib[]){
        for (T var : atrib) {
            atributos.add(var);
        }
    }

    public Tupla(){

    }

    //========================getters n setters=======================

    public T get(int i){
        return atributos.get(i);
    }
    public List<T> getAll(){
        return atributos;
    }

    public void put(T attr, int i){
        atributos.add(i, attr);
    }

    public void put(T attr){
        atributos.add(attr);
    }

    public void put(T attr[]){
        for (T v : attr) {
            atributos.add(v);
        }
    }

    public int getTamanho(){
        return atributos.size();
    }

    @Override
    public String toString() {
        return "\n" + atributos.toString();
    }
    
    public boolean contains(T c) {
		return atributos.contains(c);

	}
}