/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
 
package estatistics.impl;

/**
 * 
 * An implementation of the model object '<em><b>State</b></em>'.
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.StateImpl#getTipo <em>Tipo</em>}</li>
 *   <li>{@link estatistics.impl.StateImpl#getTabela <em>Tabela</em>}</li>
 * </ul>
 *
 * 
 */
public class State {
	
	/**
	 * The cached value of the '{@link #getTipo() <em>Tipo</em>}' attribute.
	 * 
	 * 
	 * @see #getTipo()
	 * 
	 * @ordered
	 */
	protected String tipo = "QUANTI";

	/**
	 * The cached value of the '{@link #getTabela() <em>Tabela</em>}' attribute.
	 * 
	 * <p> Tabela esta provisorio... mudar para uma estrutura de dados de 
	 * forma a facilitar a pesquisa de dados </p>
	 * 
	 * @see #getTabela()
	 * 
	 * @ordered
	 */

	

	/**
	 * 
	 * 
	 * 
	 */
	protected State() {

	}

	// ====================================getters e setters=================================================

	/**
	 * 
	 * 
	 * 
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * 
	 * 
	 * 
	 */
	public void setTipo(String newTipo) {
		
	}

	/**
	 * 
	 * 
	 * 
	 */
	public void setTabela(String newTabela) {
		
	}

}
