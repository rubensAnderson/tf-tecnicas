/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import javax.swing.JLabel;

/**
 * 
 * An implementation of the model object '<em><b>ResUnivariado</b></em>'.
 * 
 *
 * 
 */
public class ResUnivariado extends Resultado {
	/**
	 * 
	 * Contrutor da classe resultado Univariado. Recebe
	 * uma string que pode ser um numero e salva nos componentes a serem exibidos
	 * @param res resultado
	 * 
	 */
	public ResUnivariado(String res) {
		JLabel label = new JLabel(res);
		componentes.put(res, label);
		painel.add(label);
	}
}
