
/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import javax.swing.ButtonGroup;

import estatistics.impl.AbstractFactoryConjOperation;
import estatistics.impl.AbstractFactoryFacade;
import estatistics.impl.ConjOperations;

/**
 * 
 * An implementation of the model object '<em><b>PainelOperationObserver</b></em>'.
 * 
 * Armazena o conjunto de operacoes
 * 
 */
public class PainelOperationObserver extends estatistics.impl.ViewObserver{
    
    protected ConjOperations operations;
    protected String id = "0";
    protected String tipo = "ALL";
    protected ButtonGroup bg;
	//===============================inicio=============================================
	
	/**
     *	inicializador da PainelOperationObserver
	* Inicializa a tabela e a facade supondo dados quantitativos. 
    * quando a tabela for inserida, ele decide qual tipo de dado realmente eh.
    @param ide identificador da facade e tabela.
	*/
	public PainelOperationObserver(String ide){
        System.out.println("- Iniciando O painel de operacoes...");
        id = ide;
        //tipo = "QUANTI";
        if(!facade.containsKey(ide)){
            facade.put(ide, AbstractFactoryFacade.createFacadeQuanti());
        }        
        facade.get(ide).attatch(id, this);
        operations = AbstractFactoryConjOperation.create("ALL");
    }
    //===============================getters n setters=============================================

    public ConjOperations getConjOperations(){
        return operations;
    }
    
    //===============================negocio=============================================

    public int getNumParam(String id, String op) {
    	if(operations == null) {
    		System.out.println("!!! Operations null !!! (PainelOperation::getNumParam)");
    		return 0;
    	}
    
    	if(operations.getOperation(op) == null) {
    		System.out.println("!!! operacao nulla !!! (PainelOperation::getNumParam)");
    		return 0;
    	}
    	
		return operations.getOperation(op).getNumParam();
	}
    /**
     * Caso o tipo de dados mude, ele deve se atualizar
     */
    public void atualizar(String novotipo){
    	System.out.println("== atualizando painel de operacoes ==");
    	if(novotipo == null) {
    		return;
    	}
        String tt = "ALL";
    	tipo = novotipo;
        tt = tipo;
        
        System.out.println("-- novo tipo de operacoes: " + tt);
        	
        //criando a nova facade
        facade.put(id, AbstractFactoryFacade.create(tt));
        
        // atracando com o observador...
        facade.get(id).attatch(id, this);
        
        // atualizando o novo conjunto de operacoes...
        //facade.get(id).atualizaConjOperation(tt);        
        //pegando o conjunto de operacoes...
        operations = facade.get(id).getConjOperations();
        if(operations == null) {
        	System.out.println("!!! operations ta nulo !!! (PainelOperations::atualizar)");
        }
    }
    
	/**
	* implementacao do metodo abstrato da classe Observer
	* 
	*/
	public void notifyObs(){
        System.out.println("==========voltando a view operations===========");
        System.out.println("- painel operation sendo notificada...");
        String novotipo = facade.get(id).inferDataType(id);
        
        System.out.println("-- Operations: novo tipo: " + novotipo);
        System.out.println("-- Operations: antigo tipo: " + tipo);
        
        if(novotipo != tipo) {
        	System.out.println("!!! novo tipo de dados achado ao ser notificado !!!");
        	tipo = novotipo;
        	atualizar(novotipo);                    	
        }
	}

}