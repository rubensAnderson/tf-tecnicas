/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * 
 *
 * 
 */
public class FacadeAll extends Facade{
	/**
	 * 
	 * 
	 * 
	 */
	protected FacadeAll() {
		System.out.println("- Criando a facadeAll...");
		conjoperations = AbstractFactoryConjOperation.createConjOperationAll();
	}

	@Override
	public String toString() {
		return "facade all";
	}

}
