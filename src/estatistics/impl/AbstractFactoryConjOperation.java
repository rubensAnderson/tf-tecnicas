/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

/**
 * 
 * 
 *
 * 
 */
public abstract class AbstractFactoryConjOperation {
	
	/**
	 * 
	 * <p> deve retornar um novo conjunto de operacoes qualitativas </p>
	 * 
	 * 
	 */
	public static ConjOperationQuali createConjOperationQuali() {
		return new ConjOperationQuali();
	}
	
	/**
	 * 
	 * <p> deve retornar um novo conjunto de operacoes quantitativas </p>
	 * 
	 * 
	 */
	public static ConjOperationQuanti createConjOperationQuanti() {
		return new ConjOperationQuanti();
	}
	/**
	 * 
	 * <p> deve retornar um novo conjunto de operacoes quantitativas </p>
	 * 
	 * 
	 */
	public static ConjOperationAll createConjOperationAll() {
		return new ConjOperationAll();
	}

	public static ConjOperations create(String tipo){
		if(tipo == "QUANTI"){
			return createConjOperationQuanti();
		}
		else if(tipo == "QUALI"){
			return createConjOperationQuali();
		}
		return createConjOperationAll();
	}

}
