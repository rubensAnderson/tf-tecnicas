/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */

package estatistics.impl;

import java.util.Hashtable;

/**
 * 
 * An implementation of the model object '<em><b>PainelResultadoObserver</b></em>'. <br>
 * This class is <b>Singleton</b>
 *
 * 
 */
public abstract class Model{
    protected static Hashtable <String, estatistics.impl.Tabela> tabelas = new Hashtable <String , estatistics.impl.Tabela> ();

    //=========================getters n setters==========================
    
    /**
     * Cada tabela deve ter um id e quem chama esta funcao 
     * deve conhecer o id da sua tabela alvo.
     * @return retorna uma tabela com identificador = id
     */
    public static Tabela getTabela(String id){
        if(!tabelas.containsKey(id)){
            tabelas.put(id, new Tabela(id) );
        }
        return tabelas.get(id);
    }
    
    public static void putTabela(String id, Tabela tab){
        
        tabelas.put(id, tab);
    }
    
    public static void remTabela(String id){
    	if(!tabelas.containsKey(id)){
            return;
        }
        tabelas.remove(id);
    }
    
    //=========================negocio==========================

    /**
     * Seleciona a tabela com id = id e insere um novo arquivo csv. 
     * 
     * @param id Idemtificador da tabela
     */
    public static void update(String id){
        System.out.println("- Modelo atualizando dados...");
        if(!tabelas.containsKey(id)){
            System.out.println("===tabela nao existe... criando uma nova");
            tabelas.put(id, new Tabela(id) );
        }
		tabelas.get(id).update();
	}
}
   