/**
 * @author Rubens Anderson
 * @version 1.0
 * @since 2018-05
 */
package estatistics.impl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * 
 * An implementation of the model object '<em><b>Observable</b></em>'.
 * 
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link estatistics.impl.ObservableImpl#getState <em>State</em>}</li>
 * </ul>
 *
 * 
 */
public abstract class Observable extends AbstractTableModel{
	static final long serialVersionUID = 376432;
	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * 
	 * 
	 * @see #getState()
	 * 
	 * @ordered
	 */
	protected State state;
	protected List<Observer> observadores = new ArrayList<Observer>();
	

	/**
	 * 
	 * @return estado atual.
	 * 
	 */
	public State getState() {
		System.out.println("- observavel pegando estado...");	
		return state;
	}

	/**
	 * Atribui o novo estado ao atual.
	 * 
	 * @param newState Novo estado do modelo.
	 */
	public void setState(State newState) {
		state = newState;
	}

	/**
	 * inscreve o novo observador a lista de espectadores.
	 * 
	 * 
	 */
	public void attatch(Observer obs) {
		System.out.println("- observavel inscrevendo obs...");
		if(!observadores.contains(obs)){
			System.out.println("==Realmente inscreveu...");
			observadores.add(obs);
			System.out.println(observadores.size());
		}
	}

	/**
	 * 
	 * 
	 * 
	 */
	public void dettatch(Observer obs) {
		observadores.remove(obs);
	}
	
	/**
	 * 
	 * <p> Ao modificar o modelo, avisar a todos os observadores </p>
	 * 
	 * 
	 */
	protected void notifyObs() {
		System.out.println("- observavel notificando... Numero de inscritos: " + observadores.size());
		for(Observer o : observadores){
			System.out.println(".");
			o.notifyObs();
		}
	}

	@Override
	public String toString() {
		return "top";
	}
} 
